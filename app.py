from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:040464Aba@localhost/Resume'
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    first_name = db.Column(db.String(255), nullable=False)
    last_name = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(255))
    email = db.Column(db.String(255), nullable=False, unique=True)

class CV(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    introduction = db.Column(db.Text)
    education = db.Column(db.Text)

class Experience(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    years = db.Column(db.String(20))
    header = db.Column(db.String(255))
    content = db.Column(db.Text)

@app.route('/users')
def display_users():
    users = User.query.all()
    return render_template('users.html', users=users)

@app.route('/cv')
def display_cv():
    cvs = CV.query.all()
    return render_template('cv.html', cvs=cvs)

@app.route('/experience')
def display_experience():
    experiences = Experience.query.all()
    return render_template('experience.html', experiences=experiences)


if __name__ == '_main_':
    with app.app_context():
        db.create_all()

        users_data = [
            {
                'first_name': 'John',
                'last_name': 'Doe',
                'username': 'johnd',
                'password': 'password1',
                'phone': '123456789',
                'email': 'johnd@example.com'
            },
            {
                'first_name': 'Jane',
                'last_name': 'Smith',
                'username': 'janes',
                'password': 'password2',
                'phone': '987654321',
                'email': 'janes@example.com'
            },
            {
                'first_name': 'Alice',
                'last_name': 'Johnson',
                'username': 'alicej',
                'password': 'password3',
                'phone': '555555555',
                'email': 'alice@example.com'
            },
            {
                'first_name': 'Bob',
                'last_name': 'Brown',
                'username': 'bobb',
                'password': 'password4',
                'phone': '666666666',
                'email': 'bob@example.com'
            },
            {
                'first_name': 'Eva',
                'last_name': 'Lee',
                'username': 'evalee',
                'password': 'password5',
                'phone': '777777777',
                'email': 'eva@example.com'
            },
            {
                'first_name': 'Michael',
                'last_name': 'Johnson',
                'username': 'michaelj',
                'password': 'password6',
                'phone': '888888888',
                'email': 'michael@example.com'
            }
        ]


        for user_data in users_data:
            new_user = User(**user_data)
            db.session.add(new_user)

        db.session.commit()

    app.run(debug=True)
