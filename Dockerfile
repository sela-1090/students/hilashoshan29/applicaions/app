FROM python:3.8

COPY . .

RUN pip install flask

#EXPOSE 5000

CMD ["python3", "app.py"]
